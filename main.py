import os
import pygame as pg
from grid import *

text_color = (255, 255, 255)

win_score = 5
grid_size = (50, 60)
tile_size = 15

# screen variables
min_screen_coord = (0, 0)
max_screen_coord = (960, 900)

time_coord = [max_screen_coord[0] / 2, 32]
FPS = 60

font_file = 'SadanaSquare.ttf'


def main():
    pg.init()
    pg.display.set_caption('Tron')
    screen = pg.display.set_mode(max_screen_coord)

    grid_background = pg.image.load(
        os.path.join('sprites', 'grid_background.png')).convert()
    background = pg.Surface(screen.get_size())
    background.fill((10, 10, 10))

    # load font
    font = pg.font.Font(os.path.join('fonts', font_file), 38)

    while True:  # start screen
        grid = Grid(grid_size, tile_size, grid_background)
        grid_coord = ((max_screen_coord[0] - grid.get_width()) // 2,
                      (max_screen_coord[1] - grid.get_height()) // 2 + 40)
        score_coord = (grid_coord[0], grid_coord[1] - 50)

        # Draw
        screen.blit(background, min_screen_coord)
        grid.show(screen, grid_coord)
        text_surface = write(font, 'PRESS SPACE TO START', text_color)
        screen.blit(text_surface, (time_coord[0] - text_surface.get_width() / 2, time_coord[1]))
        pg.display.flip()

        # wait for input
        while True:
            event = pg.event.wait()
            if event.type == pg.KEYDOWN and event.key == pg.K_SPACE:  # start the game
                break
            if (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE) or event.type == pg.QUIT:
                exit()

        total_time = 0.0
        clock = pg.time.Clock()
        game_over = False

        while not game_over:  # main game loop
            game_over = grid.is_game_over(win_score)

            elapsed_time = clock.tick(FPS) / 1000.0  # convert to seconds
            total_time += elapsed_time

            # check input
            for event in pg.event.get():
                grid.check_input(event)
                if event.type == pg.QUIT:
                    exit()
                elif event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                    game_over = True

            grid.update(elapsed_time)

            # Draw
            screen.blit(background, min_screen_coord)
            grid.show(screen, grid_coord)
            grid.show_score(screen, font, score_coord)
            time_string = 'TIME ' + '{0:02d}'.format(int(total_time // 60)) \
                          + ":" + '{0:02d}'.format(int(total_time % 60))
            time_surface = write(font, time_string, text_color)
            screen.blit(time_surface, (time_coord[0] - time_surface.get_width() / 2, time_coord[1]))
            pg.display.flip()

        text_surface = write(font, 'GAME OVER', text_color)
        screen.blit(text_surface, ((max_screen_coord[0] - text_surface.get_width()) / 2,
                                   max_screen_coord[1] / 2))
        pg.display.flip()
        clock.tick(0.5)


if __name__ == '__main__':
    main()

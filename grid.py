import pygame as pg
from player import Player

# up, down, left, right
keys = ((pg.K_w, pg.K_s, pg.K_a, pg.K_d),
        (pg.K_UP, pg.K_DOWN, pg.K_LEFT, pg.K_RIGHT))


def write(font, message, color):
    text = font.render(str(message), True, color)
    text = text.convert_alpha()

    return text


class Grid:
    def __init__(self, grid_size, tile_size, background):
        """
        :param grid_size: (rows, columns)
        :param tile_size: integer
        :param background: pygame surface
        """
        self.round_pause = 0.0

        # create a rows x columns matrix
        self.grid = []
        for i in range(grid_size[0]):
            self.grid.append([-1] * grid_size[1])
        self.width = grid_size[1] * tile_size
        self.height = grid_size[0] * tile_size

        self.background = background
        pg.transform.scale(self.background, (self.width, self.height), self.background)

        # create players
        self.players = []
        player_positions = ((grid_size[0] // 2, grid_size[1] // 4),
                            (grid_size[0] // 2, 3 * grid_size[1] // 4))
        player_colors = ((255, 0, 0), (0, 0, 255))
        for i in range(2):
            self.players.append(
                Player(player_colors[i], player_positions[i], (tile_size, tile_size)))

        self.grid_size = tuple(grid_size)
        self.tile_size = tile_size
        self.reset()

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def check_input(self, event):
        for i in range(len(self.players)):
            self.players[i].check_input(event, keys[i])

    def update(self, time):
        if self.round_pause > 0:  # pause the game after every round
            self.round_pause -= time
            return
        else:
            self.round_pause = 0
        active_players = 0
        players_moved = False

        # move all players that are active
        for player in self.players:
            if player.is_active():
                players_moved = player.move(time)

        if players_moved:
            for i in range(len(self.players)):
                # search for players that moved to the same tile simultaneously
                # and disable them
                for j in range(len(self.players)):
                    if self.players[i].get_position() == self.players[j].get_position() and i != j:
                        self.players[i].disable()
                        self.players[j].disable()

                if self.players[i].is_active():
                    position = self.players[i].get_position()
                    # check if tile is out of bounds or occupied
                    if position[0] < 0 or position[0] >= self.grid_size[0] or \
                        position[1] < 0 or position[1] >= self.grid_size[1] or \
                            self.grid[position[0]][position[1]] != -1:
                        self.players[i].disable()
                    else:
                        self._set_cell_id(position, i)
                        active_players += 1

            if active_players <= 1:
                for player in self.players:
                    if player.is_active():
                        player.increase_score()
                self.round_pause = 3  # seconds
                self.reset()

    def _set_cell_id(self, position, player_id):
        """ Change the value of the cell to player_id """
        self.grid[position[0]][position[1]] = player_id

    def is_game_over(self, win_score):
        for player in self.players:
            if player.get_score() == win_score:
                return True

    def reset(self):
        """
        Clear the grid and reset players state
        """
        for i in range(self.grid_size[0]):
            for j in range(self.grid_size[1]):
                self.grid[i][j] = -1
        for i in range(len(self.players)):
            self.players[i].reset()
            self._set_cell_id(self.players[i].get_position(), i)

    def show(self, screen, coord):
        screen.blit(self.background, coord)
        x, y = coord
        start_x = x
        for i in range(self.grid_size[0]):  # for every row
            for j in range(self.grid_size[1]):  # for every column
                tile_value = self.grid[i][j]
                if tile_value != -1:  # if tile is not empty
                    screen.blit(self.players[tile_value].get_tile(), (x, y))
                x += self.tile_size
            x = start_x
            y += self.tile_size

    def show_score(self, screen, font, coord):
        text_list = []
        for i in range(len(self.players)):
            text_list.append(write(font, 'PLAYER ' + str(i + 1) + ': ' +
                                   str(self.players[i].get_score()),
                                   self.players[i].get_color()))
        screen.blit(text_list[0], coord)
        screen.blit(text_list[1], (coord[0] + self.width - text_list[1].get_width(), coord[1]))
        if self.round_pause > 0:
            pause_text = write(font, f'{self.round_pause:.0f}', (255, 255, 255))
            screen.blit(pause_text, (coord[0] + (self.width - pause_text.get_width()) / 2, coord[1]))

import pygame as pg

UP = 0
DOWN = 1
LEFT = 2
RIGHT = 3


class Player:
    move_time = 0.07  # seconds
    moves = ((-1, 0), (1, 0), (0, -1), (0, 1))  # up, down, left, right

    def __init__(self, color, position_index, tile_size):
        """
        :param color: (r, g, b)
        :param position_index: grid index position (row, column)
        :param tile_size: grid tile size (width, height)
        """
        self.active = True
        self.score = 0
        self.elapsed_time = 0.0
        self.color = color

        self.tile = pg.Surface(tile_size)
        self.tile.fill(color)

        self.start_position = tuple(position_index)
        self.position_index = list(position_index)
        self.move_type = Player.moves[UP]

    def turn_up(self):
        if self.move_type != Player.moves[DOWN]:
            self.move_type = Player.moves[UP]

    def turn_down(self):
        if self.move_type != Player.moves[UP]:
            self.move_type = Player.moves[DOWN]

    def turn_left(self):
        if self.move_type != Player.moves[RIGHT]:
            self.move_type = Player.moves[LEFT]

    def turn_right(self):
        if self.move_type != Player.moves[LEFT]:
            self.move_type = Player.moves[RIGHT]

    def move(self, time):
        """
        :param time:
        :return: True if player moved
        """
        self.elapsed_time += time
        if self.elapsed_time >= self.move_time:
            self.elapsed_time = 0.0
            self.position_index[0] += self.move_type[0]
            self.position_index[1] += self.move_type[1]
            return True
        return False

    def increase_score(self):
        self.score += 1

    def get_score(self):
        return self.score

    def check_input(self, event, keys):
        """
        Check player input
        :param event: pygame event
        :param keys: (up, down, left, right)
        """
        keys_pressed = pg.key.get_pressed()
        if event.type == pg.KEYDOWN:
            if keys_pressed[keys[0]]:
                self.turn_up()
            elif keys_pressed[keys[1]]:
                self.turn_down()
            elif keys_pressed[keys[2]]:
                self.turn_left()
            elif keys_pressed[keys[3]]:
                self.turn_right()

    def get_tile(self):
        return self.tile

    def get_color(self):
        return self.color

    def get_position(self):
        return self.position_index

    def is_active(self):
        return self.active

    def disable(self):
        self.active = False

    def reset(self):
        self.active = True
        self.elapsed_time = 0.0
        self.move_type = Player.moves[UP]
        self.position_index = list(self.start_position)
